### To Run:
1. Run `$ npm install`.
2. Run `$ bower install`.
3. Place api key and app id on `src\core\dev-exam.core.constants.js`.
4. Place Google Maps api key on `src\index.html`.
5. Run `gulp`.
6. Open `localhost:8000` on browser.