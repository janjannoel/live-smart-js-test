const gulp = require('gulp');
const concat = require('gulp-concat');
const wiredep = require('wiredep').stream;
const sourcemaps = require('gulp-sourcemaps');
const iife = require('gulp-iife');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const ngAnnotate = require('gulp-ng-annotate');
const templateCache = require('gulp-angular-templatecache');
const connect = require('gulp-connect');
const eslint = require('gulp-eslint');

const appName = 'dev-exam';

// Default Task
gulp.task('default', [
    'wiredep',
    'copy:assets',
    'build:js',
    'build:scss',
    'build:html',
    'watch:js',
    'watch:scss',
    'watch:html',
    'connect'
]);

// Build JS
gulp.task('build:js', () =>
    gulp.src([
        'src/core/' + appName + '.core.js',
        'src/app/' + appName + '.app.js',
        'src/**/*.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(iife({
            useStrict: false
        }))
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .on('error', function(error) {
            console.log(error);
            this.emit('end');
        })
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('wwwroot'))
        .pipe(connect.reload())
);

gulp.task('watch:js', () => {
    gulp.watch(['src/**/*.js'], ['build:js']);
});

// Build SCSS
gulp.task('build:scss', () =>
    gulp.src(['src/app.scss'])
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(sass()
            .on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('wwwroot'))
        .pipe(connect.reload())
);

gulp.task('watch:scss', () => {
    gulp.watch(['src/**/*.scss'], ['build:scss']);
});

// Build HTML
gulp.task('build:html', () =>
    gulp.src([
        './src/**/*.html',
        '!src/index.html'])
        .pipe(templateCache('template.js', {
            module: appName + '.app.templates',
            standalone: true
        }))
        .pipe(gulp.dest('wwwroot'))
        .pipe(connect.reload())
);

gulp.task('watch:html', () => {
    gulp.watch([
        './src/**/*.html',
        '!src/index.html'],
        ['build:html']);
});

// Copy Assets
gulp.task('copy:assets', () =>
    gulp.src([
        './src/app/assets/**/*.*'])
        .pipe(gulp.dest('wwwroot/assets'))
);

// Wire Bower dependencies
gulp.task('wiredep', () =>
    gulp.src('./src/index.html')
        .pipe(wiredep())
        .pipe(gulp.dest('wwwroot'))
);

// Connect
gulp.task('connect', () => {
    connect.server({
        root: 'wwwroot',
        livereload: true,
        port: 8000,
        middleware: connect => {
            return [connect().use('/bower_components', connect.static('bower_components'))];
        }
    });
});




