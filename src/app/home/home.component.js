class HomeController {

    constructor(
        $scope,
        $mdSidenav,
        uiGmapIsReady,
        busStopService,
        gmapUtils
    ) {
        this.$scope = $scope;
        this.$mdSidenav = $mdSidenav;
        this.uiGmapIsReady = uiGmapIsReady;
        this.busStopService = busStopService;
        this.gmapUtils = gmapUtils;

        this.init();
    }

    init() {
        this.busStops = [];
        this.map = {
            control: {},
            center: {
                // Estimated center of London
                latitude: 51.5072049,
                longitude: -0.1276157
            },
            zoom: 15
        };

        this.uiGmapIsReady.promise().then(() => {
            this.map.instance = this.map.control.getGMap();
        });

        this.onClickBusMarker = (marker, event, model) => this.clickBusMarker(marker, event, model);
    }

    displayBusStops() {
        // Only displays bus stops within the map bounds
        const bounds = this.gmapUtils.getBounds(this.map.instance.getBounds());
        this.busStopService.getAllStops(bounds.ne, bounds.sw).then(response => {
            this.busStops = response.stops;
            this.busStops.forEach(stop => stop.icon = 'assets/bus-marker.png');
        });
    }

    clickBusMarker(marker, event, model) {
        this.selectedBusStop = model;
        this.openBusStopDetails();

        this.busStopService.getLiveBusDepartures(model.atcocode)
            .then(response => {
                this.selectedBusStop.departures = Object.values(response.departures)
                    .reduce((acc, cur) => acc.concat(cur));
            })
            .catch(() => { this.selectedBusStop.departures = [] });
    }

    openBusStopDetails() {
        this.$mdSidenav('left').open();
    }

    closeBusStopDetails() {
        this.$mdSidenav('left').close();
    }

}

angular
    .module('dev-exam.app')
    .component('home', {
        templateUrl: 'app/home/home.html',
        controller: HomeController,
        controllerAs: 'home'
    });