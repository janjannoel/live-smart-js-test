angular
    .module('dev-exam.app', [
        'dev-exam.app.templates',
        'dev-exam.core',
        'ngMaterial',
        'uiGmapgoogle-maps'
    ])
    .config(setTheme);

function setTheme($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('blue');
}