class GmapUtils {

    getBounds(gmapBounds) {
        const ne = gmapBounds.getNorthEast();
        const sw = gmapBounds.getSouthWest();

        return {
            ne: {
                latitude: ne.lat(),
                longitude: ne.lng()
            },
            sw: {
                latitude: sw.lat(),
                longitude: sw.lng()
            }
        };
    }

}

angular
    .module('dev-exam.core')
    .service('gmapUtils', GmapUtils);