class BusStopService {

    constructor(
        $q,
        $resource,
        TRANSPORT_API_CONSTANTS
    ) {
        this.$q = $q;
        this.$resource = $resource;
        this.TRANSPORT_API_CONSTANTS = TRANSPORT_API_CONSTANTS;
    }

    getLiveBusDepartures(atcocode) {
        const params = {
            api_key: this.TRANSPORT_API_CONSTANTS.api_key,
            app_id: this.TRANSPORT_API_CONSTANTS.app_id
        };

        return this.$resource(
            this.TRANSPORT_API_CONSTANTS.host +
            this.TRANSPORT_API_CONSTANTS.endpoint.stop,
            { atcocode: atcocode }
        ).get(params).$promise;
    }

    getStops(min, max, limit, page) {
        const params = {
            api_key: this.TRANSPORT_API_CONSTANTS.api_key,
            app_id: this.TRANSPORT_API_CONSTANTS.app_id,
            minlon: min.longitude,
            minlat: min.latitude,
            maxlon: max.longitude,
            maxlat: max.latitude,
            page: page,
            rpp: limit
        };

        return this.$resource(
            this.TRANSPORT_API_CONSTANTS.host +
            this.TRANSPORT_API_CONSTANTS.endpoint.stops
        ).get(params).$promise;
    }

    getAllStops(min, max) {
        let limit = 25;
        let page = 1;

        return this.getStops(min, max, limit, page).then(initResponse => {
            const promises = [];
            while ((page * limit) < initResponse.total) {
                promises.push(this.getStops(min, max, limit, ++page));
            }
            return this.$q.all(promises).then(responses => {
                responses.forEach(response => {
                    initResponse.stops = initResponse.stops.concat(response.stops);
                });
                delete initResponse.page;
                delete initResponse.rpp;

                return initResponse;
            })
        });
    }

}

angular
    .module('dev-exam.core')
    .service('busStopService', BusStopService);