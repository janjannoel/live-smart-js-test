const TRANSPORT_API_CONSTANTS = {
    host: 'http://transportapi.com',
    endpoint: {
        stops: '/v3/uk/bus/stops/bbox.json',
        stop: '/v3/uk/bus/stop/:atcocode/live.json'
    },
    // Place api key and app id here
    api_key: '6c790cc8b20f0b394dedf8ba0ff8353c',
    app_id: '915f5f01'
};

angular
    .module('dev-exam.core')
    .constant('TRANSPORT_API_CONSTANTS', TRANSPORT_API_CONSTANTS);
